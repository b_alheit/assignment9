import java.util.ArrayList;
import java.util.List;

public class HangManGame {
    private final Word guessWord;
    private StringBuilder currentGuess;
    private int triesLeft;
    private String looseMessage;

    public HangManGame(Word guessWord){
        this.triesLeft = 11;
        this.guessWord = guessWord;
        this.currentGuess = new StringBuilder(new String(new char[guessWord.length()]).replace('\0', '-'));
        looseMessage = "You're such a loser. The word was " + guessWord.toString();
    }

    public void makeGuess(String guessString){
        char guess = guessString.toCharArray()[0];
        if(triesLeft > 0) {
            List<Integer> positions = new ArrayList<Integer>();
            int currentIndex = 0;
            while (currentIndex != -1) {
                currentIndex = guessWord.indexOf(guess, currentIndex);
                if (currentIndex != -1) {
                    positions.add(currentIndex);
                    currentIndex++;
                }
            }
            if (positions.size() == 0) {
                triesLeft--;
            } else {
                for (int place : positions)
                    currentGuess.setCharAt(place, guess);
            }
        }
    }

    public int getTriesLeft(){return triesLeft;}

    public String getCurrentGuess(){return currentGuess.toString();}

    public String getGuessWord(){return guessWord.toString();}

    public String toString() {
        return "Guess word: " + guessWord
                +"\nTries left: " + triesLeft
                +"\nCurrent guess: "+ currentGuess;
    }

    public void giveUp(){
        triesLeft = 0;
    }

    public String getMessage(){
        if (triesLeft>0) return currentGuess.toString();
        else return looseMessage;

    }
}
