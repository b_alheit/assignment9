import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GiveUpListener implements ActionListener {
    private HangManGame game;
    private JTextField currentGuess;
    private JTextArea display;

    public GiveUpListener(final HangManGame game,final JTextField currentGuess, final JTextArea display){
        this.game = game;
        this.currentGuess = currentGuess;
        this.display = display;
    }

    public void actionPerformed(ActionEvent e) {
        game.giveUp();
        display.setText(game.toString());
        currentGuess.setText(game.getMessage());
    }

}
