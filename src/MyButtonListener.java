import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class MyButtonListener implements ActionListener {

    private JTextField displayTextField, readTextField;
    private JTextArea dataField;
    private Word guessWord, display;
    private StringBuilder currentGuess;
    private int triesLeft;

    public MyButtonListener(final JTextField rField, final JTextField dField, final JTextArea dataField, final Word gWord, final int triesLeft) {
        this.triesLeft = triesLeft;
        this.displayTextField = dField;
        this.readTextField = rField;
        this.dataField = dataField;
        this.guessWord = gWord;
        this.dataField.setText("The guess word is: " + gWord + "\ntries left: " + triesLeft);
        this.currentGuess = new StringBuilder(new String(new char[gWord.length()]).replace('\0', '-'));
        this.displayTextField.setText(this.currentGuess.toString());
    }

    public void actionPerformed(ActionEvent e) {
        if(triesLeft > 0) {
            char guessChar = readTextField.getText().toCharArray()[0];
            List<Integer> positions = new ArrayList<Integer>();
            int currentIndex = 0;
            while (currentIndex != -1) {
                currentIndex = guessWord.indexOf(guessChar, currentIndex);
                if (currentIndex != -1) {
                    positions.add(currentIndex);
                    currentIndex++;
                }
            }
            if (positions.size() == 0) {
                triesLeft--;
            } else {
                for (int place : positions)
                    currentGuess.setCharAt(place, guessChar);
            }

            this.dataField.setText("The guess word is: " + guessWord + "\ntries left: " + triesLeft);
            if(triesLeft>0)
                displayTextField.setText(currentGuess.toString());
            else
                displayTextField.setText("You lost :( The word was: " + guessWord);
        }
        else{
            this.dataField.setText("The guess word is: " + guessWord + "\ntries left: " + triesLeft);
            displayTextField.setText("You lost :( The word was: " + guessWord);
        }
    }


    public void setTriesLeft(int tries){this.triesLeft=tries;}
}