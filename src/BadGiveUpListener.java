import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BadGiveUpListener implements ActionListener {
    private JTextField displayTextField;
    private JTextArea dataField;
    private Word guessWord;
    private int triesLeft;

    public BadGiveUpListener(final JTextField dField, final JTextArea dataField, final Word gWord, final int triesLeft){
        this.triesLeft = triesLeft;
        this.displayTextField = dField;
        this.dataField = dataField;
        this.guessWord = gWord;
    }

    public void actionPerformed(ActionEvent e) {
        triesLeft = 0;
        this.dataField.setText("The guess word is: " + guessWord + "\ntries left: " + triesLeft);
        displayTextField.setText("You lost :( The word was: " + guessWord);
    }

}
