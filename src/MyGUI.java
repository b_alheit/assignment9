import javax.swing.*;
import java.awt.*;

public class MyGUI {



    public static void main(final String[] args) {

        WordList wl = new WordList();
        try{
            wl = WordList.readFromFile("/home/cerecam/Benjamin_Alheit/Projects/CSC1016/assignment9/src/dictionary.txt");
        }catch (Exception e){
            wl = new WordList();
            System.out.println(e);
        }

        int triesLeft = 5;
        int nWords = wl.size();
        int randomNumber = (int) (Math.random() * nWords);
        Word guessWord = wl.get(randomNumber);
        final JFrame window;
//        final JPa
        final JButton makeGuess, giveUp;
        final JTextField displayTextField, readTextField;
        final JTextArea dataField;
        final ImagePanel imagePanel;

        imagePanel = new ImagePanel("/home/cerecam/Benjamin_Alheit/Projects/CSC1016/assignment9/src/hangmandrawings/state11.GIF");


        window = new JFrame("Hangman game");
        window.setLocation(50, 100);

        readTextField = new JTextField(20);
        readTextField.setEditable(true);

        displayTextField = new JTextField(20);
        displayTextField.setEditable(false);

        dataField = new JTextArea();
        dataField.setEditable(false);

        makeGuess = new JButton("Make guess");
        makeGuess.addActionListener(new MyButtonListener(readTextField, displayTextField, dataField, guessWord, triesLeft));


        giveUp = new JButton("Give up");
        giveUp.addActionListener(new BadGiveUpListener(displayTextField, dataField, guessWord,  triesLeft));

        final Container contentPane = window.getContentPane();
        contentPane.add(makeGuess, BorderLayout.EAST);
        contentPane.add(giveUp, BorderLayout.WEST);
        contentPane.add(displayTextField, BorderLayout.SOUTH);
//        contentPane.add(dataField, BorderLayout.CENTER);
        contentPane.add(imagePanel, BorderLayout.CENTER);
        contentPane.add(readTextField, BorderLayout.NORTH);
        window.pack();
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        window.setVisible(true);
    }
}
