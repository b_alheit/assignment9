import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class HangManGUI {
    public static void main(final String[] args){
        Scanner in = new Scanner(System.in);
        WordList wl = new WordList();
        try{
            wl = WordList.readFromFile("/home/benjamin/VC_projects/CSC1016S/Assignment9/src/dictionary.txt");
        }catch (Exception e){
            wl = new WordList();
            System.out.println(e);
        }

        int nWords = wl.size();
        int randomNumber = (int) (Math.random() * nWords);
        Word guessWord = wl.get(randomNumber);

        HangManGame game = new HangManGame(guessWord);

        final JFrame window;
        final JButton makeGuess, giveUp;
        final JTextField currentGuess, input;
        final JTextArea display;
        final ImagePanel imagePanel;


        window = new JFrame("Hangman game");
        window.setLocation(50, 100);

        input = new JTextField(20);
        input.setEditable(true);

        currentGuess = new JTextField(20);
        currentGuess.setEditable(false);

        display = new JTextArea();
        display.setEditable(false);

        makeGuess = new JButton("Make guess");
        makeGuess.addActionListener(new GuessListener(game, currentGuess, input, display));


        giveUp = new JButton("Give up");
        giveUp.addActionListener(new GiveUpListener(game, currentGuess, display));


        final Container contentPane = window.getContentPane();
        contentPane.add(makeGuess, BorderLayout.EAST);
        contentPane.add(giveUp, BorderLayout.WEST);
        contentPane.add(currentGuess, BorderLayout.SOUTH);
        contentPane.add(display, BorderLayout.CENTER);
//        contentPane.add(imagePanel, BorderLayout.CENTER);
        contentPane.add(input, BorderLayout.NORTH);
        window.pack();
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        window.setVisible(true);







    }
}
