import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuessListener implements ActionListener {
    private HangManGame game;
    private JTextField currentGuess, input;
    private JTextArea display;

    public GuessListener(final HangManGame game,final JTextField currentGuess, final JTextField input, final JTextArea display){
        this.game = game;
        this.currentGuess = currentGuess;
        this.input = input;
        this.display = display;
    }

    public void actionPerformed(ActionEvent e) {
        game.makeGuess(input.getText());
        display.setText(game.toString());
        currentGuess.setText(game.getMessage());
    }

}
