import java.util.Scanner;

public class HangManDriver {
    public static void main(final String[] args){
        Scanner in = new Scanner(System.in);
        WordList wl = new WordList();
        try{
            wl = WordList.readFromFile("/home/benjamin/VC_projects/CSC1016S/Assignment9/src/dictionary.txt");
        }catch (Exception e){
            wl = new WordList();
            System.out.println(e);
        }

        int nWords = wl.size();
        int randomNumber = (int) (Math.random() * nWords);
        Word guessWord = wl.get(randomNumber);

        HangManGame game = new HangManGame(guessWord);
        String guess = "i";
        while (!guess.equals("0")){
            System.out.println("Please make guess: ");
            guess = in.next();
            game.makeGuess(guess);
            System.out.println(game);
        }
    }
}
